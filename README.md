# Pack Planner

Welcome to Pack Planner.

## Instructions


### To execute tests:

```bash
$ ./gradlew test
```
### To run application:
```bash
$ ./gradle build
$ java -jar ./build/libs/packplanner-0.0.1-SNAPSHOT.jar
```

#### Input examples:

```bash
$ ---==> NATURAL,40,500.0 
$ ---==> 1001,6200,30,9.653
$ ---==> 2001,7200,50,11.21
```
