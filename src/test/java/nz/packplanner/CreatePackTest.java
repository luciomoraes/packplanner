package nz.packplanner;

import nz.packplanner.model.Item;
import nz.packplanner.model.Pack;
import nz.packplanner.service.CreatePack;
import nz.packplanner.service.SortType;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class CreatePackTest {

    public static int itemsQuantity = 40;
    public static double itemsWeight = 500.0;
    public static List<Item> listOfItems = new ArrayList<>();

    @Test
    public void testCreatePackNatural(){

        listOfItems.add(new Item(1001, 6200, 30, 9.653));
        listOfItems.add(new Item(2001, 7200, 50, 11.21));

        SortType sortType = SortType.NATURAL;

        CreatePack createPack = new CreatePack(sortType, listOfItems);
        List<Pack> result = createPack.create(itemsQuantity, itemsWeight);

        assertEquals(2, result.size());

        assertEquals("Pack Number: 1\n" +
                     "1001,6200,30,9.653\n" +
                     "2001,7200,10,11.21\n" +
                     "Pack Length: 7200, Pack Weight: 401.69\n", result.get(0).toString());

        assertEquals("Pack Number: 2\n" +
                     "2001,7200,40,11.21\n" +
                     "Pack Length: 7200, Pack Weight: 448.4\n", result.get(1).toString());
    }

    @Test
    public void testCreatePackShortToLong(){

        listOfItems.add(new Item(1001, 6200, 30, 9.653));
        listOfItems.add(new Item(2001, 8300, 40, 15.325));
        listOfItems.add(new Item(3001, 7200, 50, 11.21));

        SortType sortType = SortType.SHORT_TO_LONG;

        CreatePack createPack = new CreatePack(sortType, listOfItems);
        List<Pack> result = createPack.create(itemsQuantity, itemsWeight);

        assertEquals(4, result.size());

        assertEquals("Pack Number: 1\n" +
                     "1001,6200,30,9.653\n" +
                     "3001,7200,10,11.21\n" +
                     "Pack Length: 7200, Pack Weight: 401.69\n", result.get(0).toString());

        assertEquals("Pack Number: 2\n" +
                     "3001,7200,40,11.21\n" +
                     "Pack Length: 7200, Pack Weight: 448.4\n", result.get(1).toString());

        assertEquals("Pack Number: 3\n" +
                     "2001,8300,32,15.325\n" +
                     "Pack Length: 8300, Pack Weight: 490.4\n", result.get(2).toString());

        assertEquals("Pack Number: 4\n" +
                     "2001,8300,8,15.325\n" +
                     "Pack Length: 8300, Pack Weight: 122.6\n", result.get(3).toString());
    }

    @Test
    public void testCreatePackLongToShort(){

        listOfItems.add(new Item(1001, 6200, 30, 9.653));
        listOfItems.add(new Item(2001, 8300, 40, 15.325));
        listOfItems.add(new Item(3001, 7200, 50, 11.21));

        SortType sortType = SortType.LONG_TO_SHORT;

        CreatePack createPack = new CreatePack(sortType, listOfItems);
        List<Pack> result = createPack.create(itemsQuantity, itemsWeight);

        assertEquals(4, result.size());

        assertEquals("Pack Number: 1\n" +
                     "2001,8300,32,15.325\n" +
                     "Pack Length: 8300, Pack Weight: 490.4\n", result.get(0).toString());

        assertEquals("Pack Number: 2\n" +
                     "2001,8300,8,15.325\n" +
                     "3001,7200,32,11.21\n" +
                     "Pack Length: 8300, Pack Weight: 481.32\n", result.get(1).toString());

        assertEquals("Pack Number: 3\n" +
                     "3001,7200,18,11.21\n" +
                     "1001,6200,22,9.653\n" +
                     "Pack Length: 7200, Pack Weight: 414.15\n", result.get(2).toString());

        assertEquals("Pack Number: 4\n" +
                     "1001,6200,8,9.653\n" +
                     "Pack Length: 6200, Pack Weight: 77.22\n", result.get(3).toString());
    }
}
