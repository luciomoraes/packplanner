package nz.packplanner.model;

import java.util.ArrayList;
import java.util.List;


public class Pack {

    public final int id;
    private final List<Item> listOfItems;
    private int maxLength;

    public Pack(int number) {
        this.id = number;
        maxLength = 0;
        listOfItems = new ArrayList<>();
    }

    public int getMaxLength() {
        return maxLength;
    }


    /**
     *
     * @return quantity of items in pack
     *
     */
    public int getQuantity() {
        return listOfItems.stream().mapToInt(Item::getQuantity).sum();
    }

    /**
     *
     * @return weight of Items in pack
     *
     */
    public double getWeight() {
        return listOfItems.stream().mapToDouble(item -> item.getQuantity() * item.getWeight()).sum();
    }

    /**
     * Add the Item into the Pack and set the maximum length of Items in the
     * Pack
     *
     * @param item The Item has to be added to the Pack
     */
    public void addItem(Item item) {
        listOfItems.add(item);
        maxLength = Math.max(maxLength, item.getLength());
    }

    /**
     *
     * @return The Pack presentation string
     */
    public String toString() {
        StringBuilder output = new StringBuilder("Pack Number: " + this.id + "\n");
        for (Item item : listOfItems) {
            output.append(item.toString());
        }
        output.append("Pack Length: ").append(getMaxLength()).append(", Pack Weight: ").append((double) Math.round(getWeight() * 100) / 100).append("\n");
        return output.toString();
    }
}
