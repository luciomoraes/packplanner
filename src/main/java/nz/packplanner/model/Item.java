package nz.packplanner.model;


public class Item {

    private final int id;
    private final Integer length;
    private int quantity;
    private final double weight;

    public Item(int id, Integer length, int quantity, double weight) {
        this.id = id;
        this.length = length;
        this.quantity = quantity;
        this.weight = weight;
    }

    public int getId() { return id; }

    public Integer getLength() {
        return length;
    }

    public int getQuantity() {return quantity; }

    public double getWeight() {
        return weight;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * Presentation of the Item object
     *
     * @return The string with Item's parameters
     */
     @Override
     public String toString() {
         return "" + id + "," + length + "," + quantity + "," + weight + "\n";
     }
}
