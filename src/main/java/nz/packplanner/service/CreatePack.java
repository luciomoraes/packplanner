package nz.packplanner.service;

import nz.packplanner.model.Item;
import nz.packplanner.model.Pack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

//public record CreatePack(SortType sortType,
//                         List<Item> listOfItems) {
public class CreatePack {

    private final SortType sortType;
    private final List<Item> listOfItems;
    static Checker checker = new Checker();
    public CreatePack(SortType sortType, List<Item> listOfItems) {
        this.sortType = sortType;
        this.listOfItems = listOfItems;
    }


    /**
     * @param itemsQuantity
     * @param itemsWeight
     * @return
     */
    public List<Pack> create(int itemsQuantity, double itemsWeight) {
        sort();
        return sortPack(itemsQuantity, itemsWeight);
    }

    /**
     * Sort the list by sortType
     */
    private void sort() {
        switch (sortType) {
            case NATURAL:
                break;
            case SHORT_TO_LONG:
                listOfItems.sort(checker);
                Collections.reverse(listOfItems);
                break;
            case LONG_TO_SHORT:
                listOfItems.sort(checker);
                break;
        }
    }

    /**
     *
     * Sorts the Items into the Packs
     *
     */
    private List<Pack> sortPack(int itemsQuantity, double itemsWeight) {
        ArrayList<Pack> result = new ArrayList<>();

        int packNumber = 1;
        Pack pack = new Pack(packNumber);

        int quantity;
        double weight;

        for (Item item : listOfItems) {
            
            while (item.getQuantity() > 0) {

                quantity = Math.min(itemsQuantity - pack.getQuantity(), item.getQuantity());
                weight = Math.min(itemsWeight - pack.getWeight(), item.getQuantity() * item.getWeight());
                quantity = Math.min(quantity, (int) (weight / item.getWeight()));

                if (quantity > 0) {
                    item.setQuantity(item.getQuantity() - quantity);
                    Item newItem = new Item(item.getId(), item.getLength(), quantity, item.getWeight());
                    pack.addItem(newItem);
                } else {
                    packNumber+=1;
                    result.add(pack);
                    pack = new Pack(packNumber);
                }
            }
        }

        result.add(pack);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreatePack that = (CreatePack) o;
        return sortType == that.sortType && Objects.equals(listOfItems, that.listOfItems);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sortType, listOfItems);
    }

    @Override
    public String toString() {
        return "CreatePack[" +
                "sortType=" + sortType + ", " +
                "listOfItems=" + listOfItems + ']';
    }


}
