package nz.packplanner.service;

public enum SortType {
    NATURAL,
    SHORT_TO_LONG,
    LONG_TO_SHORT
}
