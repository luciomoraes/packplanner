package nz.packplanner.service;

import nz.packplanner.model.Item;

import java.util.Comparator;

class Checker implements Comparator<Item> {
    @Override
    public int compare(Item o1, Item o2) {
        if(o1.getLength() > o2.getLength()) {
            return -1;
        }else if(o1.getLength() < o2.getLength()) {
            return 1;
        }
        else {
            return 0;
        }
    }
}