package nz.packplanner.controller;

import nz.packplanner.service.CreatePack;
import nz.packplanner.service.SortType;
import nz.packplanner.model.Item;
import nz.packplanner.model.Pack;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class PackPlanner {

    public static void main(String[] args) throws IOException {

        SortType sortType = SortType.NATURAL;
        int itemsQuantity = 0;
        float itemsWeight = 0;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Scanner in = new Scanner(System.in);
        String s;
        System.out.print("Sort order: NATURAL, SHORT_TO_LONG, LONG_TO_SHORT.\n" +
                         "#Input Format:\n" +
                         "[Sort order],[max pieces per pack],[max weight per pack].\n" +
                         "---==> ");

        s = in.next();

        String[] params = s.split("\\,");

        sortType = SortType.valueOf(params[0]);
        itemsQuantity = Integer.parseInt(params[1]);
        itemsWeight = Float.parseFloat(params[2]);

        System.out.println(new StringBuilder().append("sortType = ")
                .append(sortType).append(System.lineSeparator())
                .append("itemsQuantity = ")
                .append(itemsQuantity)
                .append(System.lineSeparator())
                .append("itemsWeight = ").append(itemsWeight));
        System.out.print("# Input format for items: (comma separated)\n" +
                         "[item id],[item length],[item quantity],[piece weight].\n" +
                         "Empty row - end of the list.\n" +
                         "---==> ");

        s = br.readLine();
        List<Item> listOfItems = new ArrayList<>();
        while (s.length() > 0) {
            params = s.split("\\,");

            try {
                listOfItems.add(new Item(Integer.parseInt(params[0]),
                        Integer.parseInt(params[1]), Integer.parseInt(params[2]),
                        Double.parseDouble(params[3])));
            } catch (NumberFormatException e) {
                System.out.println("Something wrong with the numbers you entered!\n" +
                                   "Please, make sure you folow the instruction.\n");
                System.exit(1);
            }
            System.out.println("'" + s + "' ");
            System.out.print("---==> ");
            s = br.readLine();
        }

        CreatePack createPack = new CreatePack(sortType, listOfItems);
        List<Pack> result = createPack.create(itemsQuantity, itemsWeight);

        result.stream().map(Pack::toString).forEach(System.out::println);
    }
}
